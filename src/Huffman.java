
import java.math.BigInteger;
import java.util.*;
// Töö tegemisel kasutatud näide: https://algs4.cs.princeton.edu/55compression/Huffman.java.html

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {

   /**
    * Node and tree.
    * Nodes differ being roots and leaves
    */
   private static class Node implements Comparable<Node> {
      private final Character ch;
      private final int freq;
      private final Node left, right;

      /**
       * Constructor to build Node
       * @param ch character (Name of node)
       * @param freq frequency of occurences
       * @param left left node
       * @param right right node
       */
      Node(Character ch, int freq, Node left, Node right) {
         this.ch    = ch;
         this.freq  = freq;
         this.left  = left;
         this.right = right;
      }

      /**
       * Check node about being leaf node
       * @return true if it is leaf node, else return false
       */
      private boolean isLeaf() {
         return left == null && right == null;
      }


      @Override
      public int compareTo(Node that) {
         return this.freq - that.freq;
      }

      @Override
      public String toString() {
         return "Node{" +
                 "ch='" + ch + '\'' +
                 ", freq=" + freq +
                 '}';
      }
   }

   private Node root;
   private HashMap<Byte, Integer> frequencies;
   private int bitLength;
   private int leadinZeroes;

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
      if (original == null){
         throw new RuntimeException("Use correct bytearray");
      }
      this.bitLength = 0;
      this.root = null;

   }

   /** Length of encoded data in bits. 
    * @return number of bits
    */
   public int bitLength() {
      return this.bitLength;
   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {

      HashMap<Byte, Integer> frequencies = createFrequencies(origData);

      if (frequencies.size() == 1){
         Node node = new Node(getChar(origData[0]), 1, null, null);
         this.root = new Node('R', 1, node, null);
         this.bitLength = 1;
         this.leadinZeroes = origData.length - 1;
         return new BigInteger("0", 2).toByteArray();
      }

      this.root = buildTree(frequencies);

      String[] st = new String[256];
      buildCode(st, root, "");

      StringBuilder encoded = new StringBuilder();

      for (int i = 0; i < origData.length; i++) {
         char c = getChar(origData[i]);
         String code = st[c];
         for (int j = 0; j < code.length(); j++) {
            if (code.charAt(j) == '0') {
               encoded.append("0");
            }
            else if (code.charAt(j) == '1') {
               encoded.append("1");
            }
            else throw new IllegalStateException("Illegal state");
         }
      }
      this.bitLength = encoded.length();
      this.leadinZeroes = leadingZeroes(encoded.toString());

      return  new BigInteger(encoded.toString(), 2).toByteArray();
   }

   /**
    * Get leading zeroes that are lost in conversion
    * @param str binary string
    * @return count of leading zeroes
    */
   private int leadingZeroes(String str){
      int zeroCount = 0;
      for (char c : str.toCharArray()) {
         if (c != '0'){
            return zeroCount;
         }else{
            zeroCount++;
         }
      }
      return zeroCount;
   }

   /**
    * Make a table from symbols and encodings
    * @param st array of binary codes
    * @param node
    * @param s
    */
   private static void buildCode(String[] st, Node node, String s) {
      if (!node.isLeaf()) {
         buildCode(st, node.left,  s + '0');
         buildCode(st, node.right, s + '1');
      }
      else {
         st[node.ch] = s;
      }
   }

   /**
    * Helper method to get character of byte
    * @param ba byte
    * @return character
    */
   private char getChar(byte ba){
      String str = new String(new byte[] { ba });
      return str.charAt(0);
   }

   /**
    * Build Hoffman tree from frequencies
    * @param freq Map of frequencies
    * @return root node
    */
   private static Node buildTree(HashMap<Byte, Integer> freq) {

      ArrayList<Node> leaves = new ArrayList();
      for (Map.Entry<Byte, Integer> entry : freq.entrySet()) {
         String str = new String(new byte[] { entry.getKey() });
         Character c = str.charAt(0);
         leaves.add(new Node(c, entry.getValue(), null, null));
      }

      Collections.sort(leaves);
      Collections.reverse(leaves);

      PriorityQueue<Node> pq = new PriorityQueue<>(leaves);

      while (pq.size() > 1) {
         Node left  = pq.poll();
         Node right = pq.poll();
         Node parent = new Node('\0', left.freq + right.freq, left, right);
         pq.add(parent);
      }
      return pq.poll();
   }

   /**
    * Generating map of frequencies
    * @param origData
    * @return map of frequencies
    */
   private HashMap<Byte, Integer> createFrequencies(byte [] origData){
      HashMap<Byte, Integer> frequencies = new HashMap<>();
      for (byte data : origData) {
         if (frequencies.containsKey(data)){
            frequencies.put(data, frequencies.get(data) + 1);
         }else{
            frequencies.put(data, 1);
         }
      }
      return frequencies;
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {

      StringBuilder binary = new StringBuilder();
      if (leadinZeroes > 0){
         for (int i = 0; i < leadinZeroes; i++) {
            binary.append("0");

         }
      }
      binary.append(new BigInteger(encodedData).toString(2));
      Node node = this.root;
      StringBuilder decoded = new StringBuilder();
      for (char bin : binary.toString().toCharArray()) {

         if (!node.isLeaf()){
            if (bin == '1'){
               node = node.right;
            }else if(bin == '0'){
               node = node.left;
            }
         }

         if (node.isLeaf()){
            decoded.append(node.ch);

            node = this.root;
         }

      }
      return decoded.toString().getBytes();
   }

   /** Main method. */
   public static void main (String[] params) {
      //String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      String tekst = "AAA";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
      // TODO!!! Your tests here!
   }
}

